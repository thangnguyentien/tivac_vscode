/*Hướng dẫn lập trình ngắt Tiva LauchPad*/
#include <stdint.h>
#include <stdbool.h>
#include "hw_types.h"
#include "hw_memmap.h"
#include "sysctl.h"
#include "gpio.h"
#include "timer.h"
#include "pin_map.h"
#include "interrupt.h"
#include "tm4c123gh6pm.h"
#include "uartstdio.h"
#include "systick.h"

/*Task 1: Tạo xung dùng ngắt Timer 0*/

int main(){
    unsigned long ulPeriod;
    SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);       //Clock config 40MHz

    /*Cấu hình IO*/
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);        //Enable GPIO Port F
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_3);     //PF3 is output

    /*Cấu hình Timer*/
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);       //Enable Timer 0
    TimerConfigure(TIMER0_BASE,  TIMER_CFG_A_PERIODIC);     //Timer 0 periodic count
    ulPeriod = (SysCtlClockGet()/10)/2;          //Cấu hình timer để có tần số là 10Hz

    /*4 bước cấu hình ngắt timer*/
    TimerLoadSet(TIMER0_BASE, TIMER_A, ulPeriod - 1);       //Set giá trị Load cho timer 
    IntEnable(INT_TIMER0A);         //Cho phép ngắt Timer0
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);        //Cấu hình ngắt time-out
    IntMasterEnable();

    while (1)
    {
        /* code */
    }
}

void Timer0IntHandler(){
    //Clear the timer interrupt
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);      
    //Read the current state of pin & write back the opposite state
    if(GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_3)){
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, 0);
    }else{
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, 1<<3);
    }
}

//Nên kiểm tra thanh ghi trạng thái khi sử dụng ngắt, đặc biệt trong trường hợp có nhiều nguồn ngắt
void Timer0IntHandler(){
    unsigned long ulstatus;         //Đặt ra biến trạng thái ngắt

    ulstatus = TimerIntStatus(TIMER0_BASE, true);       //Trả về giá trị thanh ghi trạng thái
    if (ulstatus & TIMER_TIMA_TIMEOUT)
    {
        //Clear the timer interrupt
        TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);     
        //Read the current state of pin & write back the opposite state
        if(GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_3)){
            GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, 0);
    }   else
        {
            GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, 1<<3);
        }
    }
}

/*Task 2: Ngắt System Tick */

//Biến đếm số lần ngắ
unsigned long g_ulCounter = 0;

//UART setup
void InitConsole(){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinConfigure(GPIO_PCTL_PA0_U0RX);       //Cấu hình chân RX
    GPIOPinConfigure(GPIO_PCTL_PA1_U0TX);       //Cấu hình chân TX
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0|GPIO_PIN_1);       //Thiet lap chan UART
    UARTStdioInit(0);
}

//Handler function
SysTickIntHandler(){
    g_ulCounter++;
}

int main(){

    unsigned long ulPrevCount = 0;
    //Cấu hình clock và UART
    SysCtlClockSet(SYSCTL_SYSDIV_1|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);       //Clock config 200MHz
    InitConsole();
    UARTprintf("System tick ");
    UARTprintf("\n Rate = 1 sec \n\n");
    g_ulCounter = 0;

    //Cấu hình System Tick
    SysTickPeriodSet(SysCtlClockGet());     //Period là 1
    IntMasterEnable();          //Enable interrupt for processor
    SysTickIntEnable();         //Enable System Tick interrupt
    SysTickEnable();            //Enable System Tick

    //Loop forever while Systick Running
    while (1)
    {
        //check to see if Systick interrupt count
        if (ulPrevCount != g_ulCounter)
        {
            //Print the interrupt counter
            UARTprintf("Number of interruptss: %d\r", g_ulCounter);
            ulPrevCount = g_ulCounter;
        }
        
    }
     
}