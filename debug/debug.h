#ifndef __DEBUG_H
#define __DEBUG_H

#define DEBUG

#ifdef DEBUG
    #define DBG(fmt,...) printf(fmt,##__VA_ARGS__)
#else
    #define DBG(fmt, args...)
#endif

#endif
