#include "state.h"

int main(){
    sensorInit();
    initState();
    pumpInit();
    while (1)
    {
        stateMachineUpdate();
    }
}