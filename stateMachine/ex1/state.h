/*Khai báo các trạng thái, các hàm*/
#ifndef _STATE_H_
#define _STATE_H_

typedef enum{
    PUMP_ON,
    PUMP_OFF
}state_t;

typedef enum{
    LOW_LEVEL,
    HIGH_LEVEL
}sensor_t;

void initState();
void stateMachineUpdate();
void sensorInit();
sensor_t sensorl();
sensor_t sensor2();
void pumpInit();
void pumpCtrl(int state);

#endif