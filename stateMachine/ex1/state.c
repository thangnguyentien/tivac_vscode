#include "state.h"

static state_t State;   //Khai báo một biến cho state

void initState(){
    State = PUMP_OFF;
}

void stateMachineUpdate(){
    switch (State)
    {
    case PUMP_OFF:
        if (sensor1()==LOW_LEVEL)
        {
            State = PUMP_ON;
        }
        break;
    case PUMP_ON:
        if (sensor2()==HIGH_LEVEL)
        {
            State = PUMP_OFF;
        }
    default:
        break;
    }
    
    switch (State)
    {
    case PUMP_OFF:
        pumpCtrl(PUMP_OFF);
        break;
    case PUMP_ON:
        pumpCtrl(PUMP_ON);
    default:
        break;
    }
    
}

