/*Các chương trình hướng dẫn sử dụng ngắt*/
/*Các Chương trình nhập xuất IO*/
#include <stdint.h>
#include <stdbool.h>
#include "hw_types.h"
#include "hw_memmap.h"
#include "sysctl.h"
#include "gpio.h"
#include "interrupt.h"
/*Ngắt nút nhấn
Ngắt ở PF4, thay đổi trạng thái sau mỗi lần ngắt thứ 5*/

// Enable & Set the system clock
SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN); //external XTAL, 50MHz

//Init and Config PORT and Pin
SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);    //Set clock cho port F  

//Pin PF4: Input, pull-up
GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);
GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);

//Pin PF1: Output
GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1);

/* GPIO Pin 1 on PORT F initialized to 0, RED LED is off */
GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,0);

/*Cấu hình ngắt*/
	GPIOIntDisable(GPIO_PORTF_BASE, GPIO_PIN_4);		// Disable interrupt for PF4 (in case it was enabled)
	GPIOIntClear(GPIO_PORTF_BASE, GPIO_PIN_4);		// Clear pending interrupts for PF4
	GPIOIntRegister(GPIO_PORTF_BASE, onButtonDown);		// Register our handler function for port F
	GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_4,GPIO_FALLING_EDGE);	// Configure PF4 for falling edge trigger
	GPIOIntEnable(GPIO_PORTF_BASE, GPIO_PIN_4);		// Enable interrupt for PF4

	while(1);
}

void onButtonDown(void) {
	if (GPIOIntStatus(GPIO_PORTF_BASE, false) & GPIO_PIN_4) {
		// PF4 was interrupt cause
		printf("Button Down\n");
		GPIOIntRegister(GPIO_PORTF_BASE, onButtonUp);	// Register our handler function for port F
		GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_4,GPIO_RISING_EDGE);			// Configure PF4 for rising edge trigger
		GPIOIntClear(GPIO_PORTF_BASE, GPIO_PIN_4);	// Clear interrupt flag
	}
}

void onButtonUp(void) {
	if (GPIOIntStatus(GPIO_PORTF_BASE, false) & GPIO_PIN_4) {
		// PF4 was interrupt cause
		printf("Button Up\n");
		GPIOIntRegister(GPIO_PORTF_BASE, onButtonDown);	// Register our handler function for port F
		GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_4,
			GPIO_FALLING_EDGE);			// Configure PF4 for falling edge trigger
		GPIOIntClear(GPIO_PORTF_BASE, GPIO_PIN_4);	// Clear interrupt flag
	}
}

/*
 * This is an example that shows the use of asynchronous interruptions requested from the General Purpose Inputs/Outputs (GPIO) peripheral
 * using the Tiva C launchpad and the Tivaware library.
 * In this example, the internal LEDs in PF1, PF2 and PF3 are used in addition to pins PB0, PB1, PB2 and PB3.
 * When the pins on B port are connected to ground the interrupt is attended and the LEDs are activated or desactivated.
*/

//Definitions
#define LEDS GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3
#define PINS GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3

//Declarations
void initClock(void);
void initGPIO(void);
void GPIOIntHandler(void);

//Main routine
int main(void) {
	initClock();
	initGPIO();
	return 0;
}

//Clock initialization
void initClock(void){
	SysCtlClockSet(SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ | SYSCTL_USE_PLL | SYSCTL_SYSDIV_5);
}

//GPIO initialization and interrupt enabling
void initGPIO(void){
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, LEDS);
	GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, PINS);
	GPIOPadConfigSet(GPIO_PORTB_BASE, PINS, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
	GPIOIntEnable(GPIO_PORTB_BASE, GPIO_INT_PIN_0|GPIO_INT_PIN_1|GPIO_INT_PIN_2|GPIO_INT_PIN_3);
	GPIOIntTypeSet(GPIO_PORTB_BASE, PINS, GPIO_LOW_LEVEL);
	IntPrioritySet(INT_GPIOB, 0);
	IntRegister(INT_GPIOB, GPIOIntHandler);
	IntEnable(INT_GPIOB);
	IntMasterEnable();
}

//Interrupt service routine
void GPIOIntHandler(void){
	GPIOIntClear(GPIO_PORTB_BASE, GPIO_INT_PIN_0|GPIO_INT_PIN_1|GPIO_INT_PIN_2|GPIO_INT_PIN_3);
	int state = GPIOIntStatus(GPIO_PORTB_BASE, true);
	switch (state){
		case 1:
			GPIOPinWrite(GPIO_PORTF_BASE, LEDS, 2);
			break;
		case 2:
			GPIOPinWrite(GPIO_PORTF_BASE, LEDS, 4);
			break;
		case 4:
			GPIOPinWrite(GPIO_PORTF_BASE, LEDS, 8);
			break;
		case 8:
			GPIOPinWrite(GPIO_PORTF_BASE, LEDS, 0);
			break;
	}
}
