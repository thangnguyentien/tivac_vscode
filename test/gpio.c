/*Các Chương trình nhập xuất IO*/
#include <stdint.h>
#include <stdbool.h>
#include "hw_types.h"
#include "hw_memmap.h"
#include "sysctl.h"
#include "gpio.h"

/*Chương trình 1 : Blink led*/

// Enable & Set the system clock
SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN); //external XTAL, 50MHz

//Init and Config PORT and Pin
SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);    //Set clock cho port F
GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1);     //Set Port F.01 as output
GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1, 0);   //Init pin as low

/*Application Loop*/
while (1)
{
    GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_PIN_1);  //Turn on the pin
    SysCtlDelay(1000000);       //Just system delay
    GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1, 0);   //Write pin low
    SysCtlDelay(1000000);       //Just system delay
}

/*Chương trình 2: Điều khiển LED bằng switch*/
/*Switch ở PF4, LED ở PF1*/

// Enable & Set the system clock
SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN); //external XTAL, 50MHz

//Init and Config PORT and Pin
SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);    //Set clock cho port F  

//Pin PF4: Input, pull-up
GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);
GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);

//Pin PF1: Output
GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1);

/* GPIO Pin 1 on PORT F initialized to 0, RED LED is off */
GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,0);

/*Application Code*/
while (1)
{
    int a;
    a = GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_4);
    //Read the state of PF4
    if (a)
    {
        /* Turn on the LED */
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_PIN_1);
    }else
    {
        /* Turn off the LED */
        GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,0);
    }
    // A small delay
    SysCtlDelay(1000);
}

/*Chương trình 3: LED chạy, 3 con led ở PF123 chạy theo thứ tự*/

// Enable & Set the system clock
SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN); //external XTAL, 50MHz

//Init and Config PORT and Pin
SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);    //Set clock cho port F 

//Pin PF1, PF2, PF3 : Output
GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1);
GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_2);
GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_3);

//Init Pin as low level
GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,0);
GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_2,0);
GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_3,0);

/*Application Code*/
while (1)
{
    /* turn on PF1 and delay, then turn of */
    GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,GPIO_PIN_1);
    SysCtlDelay(500000);
    GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,0);
    /* turn on PF2 and delay, then turn of */
    GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_2,GPIO_PIN_2);
    SysCtlDelay(500000);
    GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_2,0);
    /* turn on PF3 and delay, then turn of */
    GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_3,GPIO_PIN_3);
    SysCtlDelay(500000);
    GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_3,0);
}

/*Chương trình 4: Nhấn nút để thay đổi trạng thái led, nút ở PF4, LED ở PF1*/

// Enable & Set the system clock
SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN); //external XTAL, 50MHz

//Init and Config PORT and Pin
SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);    //Set clock cho port F  

//Pin PF4: Input, pull-up
GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);
GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);

//Pin PF1: Output
GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1);

/* GPIO Pin 1 on PORT F initialized to 0, RED LED is off */
GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,0);

/*Application Code*/
while (1)
{
    int ledState = 0;
    int buttonState = 0;
    ledState = GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_4);
    //Read the state of PF4
    if (buttonState)
    {
        ledState = ~(ledState);
        /* Turn on the LED */
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, ledState);
    }
    // A small delay
    SysCtlDelay(1000);
}














