/*Giải Các đề thi*/
#include <stdint.h>
#include <stdbool.h>
#include "hw_types.h"
#include "hw_memmap.h"
#include "sysctl.h"
#include "gpio.h"
#include "timer.h"
#include "pin_map.h"
#include "interrupt.h"
#include "tm4c123gh6pm.h"

/*Code máy trạng thái Moore*/
typedef enum{
    ON,
    OFF
} state_t;

typedef enum{
    HIGH,
    LOW
} sensor_t;

//function prototype
void  initState(void); 
void stateMachineUpdate(void);
void SensorsInit(void);
sensor_t sensorl(void);
sensor_t sensor2(void);
void pumpCtrlInit(void);
void pumpCtrl(int state);

//global var
static state_t State;

int main(){
    SensorsInit();
    initState();
    pumpCtrlInit()
    pumpCtrl(OFF);
    
    while (1)
    {
        stateMachineUpdate();
    }
    
}

void  initState(void){
    State = OFF;
}

void stateMachineUpdate(void){
    switch (State)
    {
    case ON:
        if (sensor1() == HIGH)
        {
            State = OFF;
        }
        
        break;
    case OFF:
        if (sensor2() == LOW)
        {
            State = ON;
        }
        break;
    default:
        break;
    }
}

//Máy trạng thái mealy

//state.h
typedef enum {S_GRIDON, S_GRIDOFF_WAIT, S_GRIDOFF, S_GRIDON_WAIT} genState_t;
typedef enum {S_LEDON, S_LEDOFF} ledState_t;
typedef enum {GRIDON, GRIDOFF} gridState_t;
#define GENON 1
#define GENOFF 0
//function prototype
void initGenState(void);
void genStateMachineUpdate(void); 
void SensorsInit(void);
gridState_t gridState(void);
void genCtrlInit(void ); 
void genCtrl(int state);

//state.c 
#include "state.h"
static genState_t genState;
static unsigned int genTimerCount;
void genStateMachineUpdate(void)
{
  switch (genState)
  {
    case S_GRIDON:
      if (gridstate() == GRIDOFF)
      {
        genState S_GRIDOFF_WAIT:
        DBG("state changed from S_GRIDON to S_GRIDOFF_WAIT \n").
        genTimerCount = 20;
      }
      break;
    case S_GRIDOFF_WAIT:
      if (gridState() == GRIDON)
      {
        genState = S_GRIDON; 
        DBG("state changed from S_GRIDOFF_WAIT to S_GRIDON \n");
      }
      else if (genTimerCount == 0)
      {
        genState = S_GRIDOFF; 
        DBG("state changed from S_GRIDOFF_WAIT to S_GRIDOFF \n");
      }
      break;
    case S_GRIDOFF:
      if (gridState() == GRIDON)
      {
        genState = S_GRIDON_WAIT; 
        DBG("state changed from S_GRIDOFF to S_GRIDON_WAIT \n"); 
        genTimerCount = 20;
      }
      break;
    case S_GRIDON_WAIT: 
      if (gridState() == GRIDOFF)
      {
        genState = S_GRIDOFF:
        DBG("state changed from S_GRIDON_WAIT to S_GRIDOFF \n");
      }
      else if (genTimerCount = 0)
      {
        genState = S_GRIDON; 
        DBG("state changed from S_GRIDON_WAIT to S_GRIDON \n");
      }
      break;
  }
  switch (genState)
  {
    case S_GRIDON:
    case S_GRIDOFF_WAIT:
      genCtrl(GENOFF); 
      break;
    case S_GRIDOFF: 
    case S_GRIDON_WAIT:
      genCtrl(GENON);
      break;
  }
}
void SysTickIntHandler(void)
{
  //
  // Update the Timer counter.
  //
  if (genTimerCount != 0)
  {
    genTimerCount--; 
    DBG("genTimerCount = %d\n",genTimerCount);
  }
}

#include "state.h" 
#ifdef DEBUG
void InitConsole(void)
{
  //
  // Enable GPIO port A which is used for UART0
  // pins.
  // TODO: change this to whichever GPIO port you
  // are using.
  //
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
  GPIOPinConfigure(GPIO_PA0_U0RX);
  GPIOPinConfigure(GPIO_PA1_U0TX);
  GRIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
  //
  // Initialize the OART for console 1/0.
  //
  UARTStdioInit(0);
#endif
  
int main(void)
{
  SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);
#ifdef DEBUG 
  InitConsole();
#endif
  // Set up the period for the SysTick timer. 
  // The SysTick timer period will 
  // be equal to the system clock, resulting in a 
  // period of 1 second.
  //
  SysTickPeriodSet(SysCtlClockGet());
  IntMasterEnable();
  SysTickIntEnable();
  SysTickEnable();
  SensorsInit();
  genCtrlInit();
  genCtrl(GENOFF);
  initGenState();
  while (1)
  {
    genStateMachineUpdate();
  }
}