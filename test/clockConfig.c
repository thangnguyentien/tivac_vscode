/*Hướng dẫn cấu hình Clock*/

/*Cấu hình Clock bắt buộc cho tất cả các chương trình*/
SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN); //external XTAL, 50MHz
/*SYSCTL_SYSDIV_3
 50 MHz SYSCTL_SYSDIV_4
 40 MHz SYSCTL_SYSDIV_5
 33.33 MHz SYSCTL_SYSDIV_6
 28.57 MHz SYSCTL_SYSDIV_7
 25 MHz SYSCTL_SYSDIV_8
 22.22 MHz SYSCTL_SYSDIV_9
 20 MHz SYSCTL_SYSDIV_10
 18.18 MHz SYSCTL_SYSDIV_11
 16.67 MHz SYSCTL_SYSDIV_12
 15.38 MHz SYSCTL_SYSDIV_13
 14.29 MHz SYSCTL_SYSDIV_14
 13.33 MHz SYSCTL_SYSDIV_15
 12.5 MHz SYSCTL_SYSDIV_16*/


 /*Sử dụng các hàm sau cho các mục đích cụ thể*/
 void SysCtlPeripheralEnable(uint32_t ui32Peripheral)
 /*SYSCTL_PERIPH_ADC0 , SYSCTL_PERIPH_ADC1,
SYSCTL_PERIPH_COMP0 , SYSCTL_PERIPH_EEPROM0,
SYSCTL_PERIPH_I2C1 , SYSCTL_PERIPH_I2C2,
SYSCTL_PERIPH_PWM0 , SYSCTL_PERIPH_PWM1,
SYSCTL_PERIPH_SSI0 , SYSCTL_PERIPH_SSI1*/

void SysCtlDelay(uint32_t ui32Count)
/*ui32Count là số lượng vòng lặp của delay, mất 3 chu kỳ cho một loop*/

void SysCtlReset(void)
/*Reset thiết bị*/

bool IntMasterEnable(void)
/*Enable interrupt cho thiết bị*/