/*Hướng dẫn cấu hình GPIO*/

/*Init port trước khi sử dụng*/

SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);    //Cau hinh su dung port F
GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);  //PF4 la input
GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1,0);        //khởi tạo giá trị là 0, đèn tắt
GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1,GPIO_PIN_1);   //bật đèn

//Một ví dụ khác
/* Set the type of the GPIO Pin, PF4 as input and PF1 as output */
GPIOPinTypeGPIOInput(GPIO_PORTF_BASE,GPIO_PIN_4);
GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);
GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE,GPIO_PIN_1);