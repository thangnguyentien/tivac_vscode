/*Các chương trình mẫu về sử dụng timer*/
/*Các Chương trình nhập xuất IO*/
#include <stdint.h>
#include <stdbool.h>
#include "hw_types.h"
#include "hw_memmap.h"
#include "sysctl.h"
#include "gpio.h"
#include "timer.h"
#include "pin_map.h"
#include "interrupt.h"
#include "tm4c123gh6pm.h"

/*Ex 13: Software PWM*/
// Enable & Set the system clock
SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN); //external XTAL, 50MHz

//Init and Config PORT and Pin
SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);    //Set clock cho port F  

//Pin PF1: Output
GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1);

/* GPIO Pin 1 on PORT F initialized to 0, RED LED is off */
GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,0);

/*Application code*/
while (1)
{
    int i;
    for (i = 0; i < 25; i++)
    {
        GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,0);
        // A small delay
        SysCtlDelay(10);
    }
    for ( i = 25; i < 100; i++)
    {
        GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,GPIO_PIN_1);
        // A small delay
        SysCtlDelay(10);
    }
}
    

/*Ex 14: Hardware PWM*/

unsigned long i;
void low_to_high(void)
{
    for(i=19;i>1;i--)
    {
        TimerMatchSet(TIMER1_BASE, TIMER_A, TimerLoadGet(TIMER0_BASE,TIMER_B)*i/20);
        TimerMatchSet(TIMER1_BASE, TIMER_B, TimerLoadGet(TIMER0_BASE,TIMER_B)*i/20);
        TimerMatchSet(TIMER0_BASE, TIMER_B, TimerLoadGet(TIMER0_BASE,TIMER_B)*i/20);
        TimerEnable(TIMER0_BASE, TIMER_B);
        TimerEnable(TIMER1_BASE, TIMER_A);
        TimerEnable(TIMER1_BASE, TIMER_B);
        SysCtlDelay(SysCtlClockGet()/50);
    }
}
void high_to_low(void)
{
    for(i=1;i<20;i++)
    {
        TimerMatchSet(TIMER1_BASE, TIMER_A, TimerLoadGet(TIMER0_BASE,TIMER_B)*i/20);
        TimerMatchSet(TIMER1_BASE, TIMER_B, TimerLoadGet(TIMER0_BASE,TIMER_B)*i/20);
        TimerMatchSet(TIMER0_BASE, TIMER_B, TimerLoadGet(TIMER0_BASE,TIMER_B)*i/20);
        TimerEnable(TIMER0_BASE, TIMER_B);
        TimerEnable(TIMER1_BASE, TIMER_A);
        TimerEnable(TIMER1_BASE, TIMER_B);
        SysCtlDelay(SysCtlClockGet()/50);
    }
}

/*Applicatipn Code*/
int main(void)
{
/* Set the clock to directly run from the crystal at 16MHz */
SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN |
SYSCTL_XTAL_16MHZ);
/* Set the clock for the GPIO Port F */
SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

/* The TIMER0 and TIMER1 peripheral must be enabled for use.*/
SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);

/* Configuring PORTF pin1,pin2,pin3 as timer pins */
GPIOPinTypeTimer(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);      //Cấu hình chân sử dụng timer ở đây là PF1 PF2 PF3
GPIOPinConfigure(GPIO_PF1_T0CCP1);      //Timer 0 PWN
GPIOPinConfigure(GPIO_PF2_T1CCP0);      //Timer 1 PWM
GPIOPinConfigure(GPIO_PF3_T1CCP1);      //Timer 2 PWM

/* Configuring the TIMER0 and TIMER1 */
TimerConfigure(TIMER0_BASE, TIMER_CFG_SPLIT_PAIR | TIMER_CFG_B_PWM);        //Timer B0 PWM, half-width timer
TimerConfigure(TIMER1_BASE, TIMER_CFG_SPLIT_PAIR | TIMER_CFG_B_PWM|TIMER_CFG_A_PWM);

/* Set the Load value of the Timers */
TimerLoadSet(TIMER0_BASE, TIMER_B, 50000);
TimerLoadSet(TIMER1_BASE, TIMER_A, 50000);
TimerLoadSet(TIMER1_BASE, TIMER_B, 50000);

/* Enable the Timers */
TimerEnable(TIMER0_BASE, TIMER_B);
TimerEnable(TIMER1_BASE, TIMER_A);
TimerEnable(TIMER1_BASE, TIMER_B);
while(1)
{
    low_to_high();
    high_to_low();
}
}

/*Ex 15: System timer Blinky*/

/*Timer Interrupt*/

int main(void)
{
    uint32_t ui32Period;
    SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);   //Clock config
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);        //IO config
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);   //PF123 Output

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);   //Enable timer 0
    TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);    //Config operation mode for timer 0
    ui32Period = (SysCtlClockGet() / 10) / 2;           
    TimerLoadSet(TIMER0_BASE, TIMER_A, ui32Period -1);  //Set the counting value for timer 0A
    IntEnable(INT_TIMER0A);     //It enables the interrupt from the source Timer0A
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);        //Cho phép sự kiện cụ thể (Timer A runout) tạo ra ngắt
    IntMasterEnable();      //Enable tất cả các ngắt
    TimerEnable(TIMER0_BASE, TIMER_A);      //Rune timer 0A
    while(1)
        {

        }
}

void Timer0IntHandler(void)
{
    // Clear the timer interrupt
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    // Read the current state of the GPIO pin and
    // write back the opposite state
    if(GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_2))
    {
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, 0);
    }
    else
    {
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, 4);
    }
}